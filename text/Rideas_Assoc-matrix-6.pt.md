---
output: 
  pdf_document: 
    toc: yes
    keep_tex: yes
---
# Gerando muitos testes automatizados

Nesta etapa do projeto **Matrizes de Associação** são geradas e exercitadas 
mais de duas centenas de testes automatizados, que no esforço de validar a 
função principal da biblioteca `assoc.matrix`.

À medida que o projeto **Matrizes de Associação** vai ganhando volume, é 
necessário trazer novos aspectos. Neste momento, será comentado o controle
de versões.

## Novas versões dos arquivos de programa

Os arquivos de programa sendo usados estão em evolução e, portando, sendo 
alterados. Para que pessoas assistindo vídeos anteriores possam ter acesso às 
versões correspondentes a eles, os arquivos em uso receberão um sufixo `2`, 
para indicar que são nova versão do arquivo anterior. Por exemplo, 
`a2f_test.R` será, neste vídeo `a2f_test.R`; `gen_tests.R` será `gen_tests2.R`
etc.

Esta forma de criar novas versões mantendo aquelas anteriores não é prática, 
e nem usada em ambientes profissionais. Na seção 
**Controle de versões dos arquivos para novos vídeos** é explicado como 
faremos a seguir, com a criação de um novo vídeo.

## Controle de versões dos arquivos para novos vídeos 

À medida que um projeto vai se desenvolvendo, novas versões dos programas vão 
sendo criadas, e as versões antigas são apagadas. Assim, a princípio, quem 
assiste a um vídeo anterior não teria como executar os programas descritos 
nele, porque os novos vídeos trouxeram novas versões para eles.

Contudo, isto não é verdade: estamos usando um software para controle de 
versões, chamado `git` que preserva as versões anteriores. E, mais do que isso,
no caso de arquivos de texto é possível inclusive ver o que foi mudado entre 
uma versão e outra. Isto é: podem-se ver detalhes das alterações de *scripts* 
do `R`, arquivos *Markdown* como este, arquivos *RMarkdown*, arquivos `.tex` e
descrições em texto `.txt`

Infelizmente, nem todos interessados em `R` tem conhecimentos sobre `git`. Por 
isso, em um próximo vídeo desta série haverá uma discussão dos conceitos do 
`R` necessários para se ter versões independentes.

Estamos identificando aplicativos gráficos simples em vários sistemas 
operacionais, para atender as necessidades até mesmo das pessoas que não 
têm muita familiaridade com a linha de comando, habitual para quem usa 
`git`. 

## Rearranjo de funções e criação de novas funções

O arquivo `a2f_test.R` -- agora salvo como `a2f_test2.R` -- tinha a função
`compare.test()`, que usava várias das outras funções presentes no arquivo
para poder fazer comparação entre a matriz de associação gerada e aquela 
esperada. Contudo, sendo uma função de teste, ficará melhor no arquivo
`gen_tests.R`, que é uma biblioteca de testes. 

A função `print.am()` do arquivo `a2f_test2.R` tem como análoga
`print.assoc.matrix()` do arquivo `gen_tests2.R`. Foi feito um *merge* (ou
"mistura") das duas funções, para que as qualidades de print.am() -- listagem
mais compacta -- se somassem às qualidades de `print.assoc.matrix()`, que 
continha um tratamento do parâmetro de entrada mais cuidadoso. Além disso, as 
duas funções poderão redirecionar sua saída para uma conexão qualquer, usando
o segundo parâmetreo `output`, que terá como *default* a saída padrão, também
conhecida como `stdout`. As duas funções continuarão em seus respectivos 
arquivos: é importante que o gerador de testes `run_gen_tests2.R` não dependa
de funções na biblioteca testas `gen_tests2.R`.

A maior parte das funções para geração de testes já está definida. Será
introduzida em `gen_tests2.R` a função `open.section()`, que permitirá marcar 
seções no arquivo de testes.

Também será criado seu par `close.section()`. A função `open.section()` 
disparará cronômetros para a os testes da seção, que serão totalizados por 
`close.section()`.

Além disso, será renomeada a função `create.header()` de `gen_tests2.R`, para
`open.tests()`, por analogia com `open.section()`. Também por analogia, ela 
iniciará cronômetros para todo teste, que serão totalizados em `close.tests()`;
além disso, iniciará também o gerador de números aleatórios.

Também `gen_tests2.R` será criada a função `gen.random.tests()` que fará uma 
chamada a `gen.word.pair()`, para obter uma cadeia linear de palavras, uma em 
cada elemento de um vetor de caracteres. Essa cadeia linear será usada como 
parâmetro em `paste0()` para criar um nome para o teste.

A função `gen.random.tests()` terá dois argumentos:

* `n.tests` que indica o número de testes a serem gerados;

* `n.words` que indica o comprimento de palavras.

A função `gen.word.pair()` devolve uma cadeia linear de palavras e um vetor 
com a frequência de ocorrência de cada uma das palavras. Em uma próxima versão
ela será usada para criar uma matriz de frequências que será então 
transformada em uma matriz de associação de palavras.

Ainda em próxima versão, `gen.word.pair()` criará não mais um vetor linear de 
palavras, mas um conjunto de linhas com várias palavras, que tentará aproximar 
as linhas de um texto.

## Estrutura do novo programa de testes `run_gen_tests2.R`

O programa `run_gen_tests2.R` será composto de 3 elementos principais:

1. chamada a `open.tests()` para iniciar contadores e abrir arquivos;

2. A seção de testes em si, com dois tipos básicos de testes:

    * Testes **básicos**, construídos "manualmente", com indicação de seus 
      elementos, construídos por `gen.simple.test()`;
      
    * Testes aleatórios, construídos através de `gen.random.tests()`. 
      Conforme visto na seção 
      **Rearranjo de funções e criação de novas funções**, essa função têm
      um parâmetro que permite descrever o número de testes aleatórios a 
      serem gerados. 
      
    De forma geral não é necessário usar um laço (ou *loop*) para gerar 
    testes, pois os testes aleatórios, que têm potencial para gerar massas
    maiores de testes, têm um laço `for` interno, que é controlado pelo 
    parâmetro `n.tests`.

3. chamada a `close.tests()` para imprimir contadores e fechar arquivos.


