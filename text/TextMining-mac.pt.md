# Minerando alguns romances de Machado de Assis

A mineração de dados encontra tópicos da línguística matemática que só se revelam em bases de dados (ou *corpus*) muito grandes.

Muitos desses trabalhos são feitos em meses de muitos esforços e reflexões especializadas. Fizemos neste vídeo uma tentativa mais modesta, de aplicar pacotes do `R` a alguns dos romances da fase madura de Machado de Assis: Memórias Póstumas de Brás Cubas,  Casa Velha, Quincas Borba, Dom Casmurro, Esaú e Jacó e Memorial de Aires.

Nossos objetivos são modestos: buscamos apenas montar nuvem de palavras (ou *word cloud*) dos romances de Machado de Assis, isoladamente e em conjunto.

Para isso usaremos os pacotes

* `tm` para todas tarefas básicas de mineração de texto; da transformação de arquivos PDF em texto até a eliminação de "palavras vazias";

* `hunspell` para *stemming* (traduzido por "stemização"), para reduzir palavras a seus radicais;

* `wordcloud` para criação de nuvens de palavras.

## Limpeza do texto

Os textos disponíveis na Internet são em geral em PDF. Felizmente, o pacote `tm` tem recursos para transformar os arquivos PDF em vetores de texto.

Isto é feito pelo programa `pdfToText.R`:

```R
# Convert a list of PDF files in a given directory to text files

library('tm')

pdfToText <- function(filePath, func = pdfFunc) {
  cat(filePath, '\n')
  
  # Reading the PDF 
  txt <- func(elem = list(uri = filePath), language = "pt")

  result <- txt$content
}

pdfFunc <- readPDF(control = list(text = "-layout"))

# Get all files

pattern <- '.pdf'
folder  <- '../data/'

files <- list.files(path = folder, pattern = pattern)

# For all files in the list

for (file in files) {
  cat(file, ' ')
  
  # Read PDF file into a character vector
  text <- pdfToText(paste(folder, file, sep = ''))

  # TODO eliminate or change UTF-8 characters that hunspell_stem() can't handle
  text <- gsub('—' , '-', text) # EM Dash becomes normal hyphen
  text <- gsub('’’', '"', text) # Double EM Dash becomes the quote
  text <- gsub('’' , "'", text) # EM Dash becomes apostrophe, aka single quote
  text <- gsub('”' , '"', text) # RIGHT DOUBLE QUOTATION MARK becomes the quote
  text <- gsub('“' , '"', text) # LEFT DOUBLE QUOTATION MARK becomes the quote
  
  
  # Create .txt file name
  base <- strsplit(file, pattern)[[1]]
  txtName <- paste(folder, base, '.txt', sep = '')
  
  # Write character vector as a text file
  textFile <- file(txtName)
  writeLines(text, textFile)
  close(textFile)
}
```

Esses vetores são a seguir transformados em arquivos. Uma inspeção dos arquivos mostra que todos têm cabeçalho adicional  (para descrição de edição etc.), que não faz parte do texto original. Infelizmente, os cabeçalhos não são padronizados e, portanto, têm que ser eliminados manualmente.

## Preparação do *corpus*

1. importação dos textos limpos em um *corpus*;

2. minusculação do texto;

3. eliminação de espaços de todo tipo;

4. eliminação de pontuação;

5. eliminação de palavras sem sentido;

6. criação de *corpus* com *stemming*;

7. gravação de *corpora* com e sem *stemming*.

## Cálculo de frequências de palavras e geração de nuvens

1. Carga dos *corpora*;

2. Cálculo de frequência de palavras para *corpus* convencional;

3. Seleção de palavras mais frequentes para *corpus* convencional;

4. Desenho de histogramas para *corpus* convencional;

5. Cálculo de frequência de palavras para *corpus* convencional;

6. Seleção de palavras mais frequentes para *corpus* convencional;

7. Desenho de histogramas para *corpus* convencional.
