---
params:
  lang: PT
lang: "`r switch(params$lang, PT = 'pt-BR', EN = 'en-US')`"
title: "Planejando o porte de `gen_tests2` para `testthat`"
author: "Hilton G. Fernandes"
date: "05/07/2021"
output: 
  pdf_document: 
    toc: yes
    toc_depth: 3
    keep_tex: yes
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## O planejamento é a alma do negócio

Um ditado antigo dizia "o segredo é a alma do negócio". Mas as pessoas 
de tecnologia da informação sabem que, na verdade, o **planejamento** é que 
a alma do negócio.

Considere, por exemplo, fazer o porte do pacote de testes `gen_tests2` 
que fizemos até aqui para o pacote `testthat`. Naturalmente, sendo compatível 
com o pacote `covr`.

Há muitos detalhes a considerar: `gen_tests2` tem por um lado uma estrutura 
similar àquela do `testthat`. Mas há diferenças importantes que exigem 
planejamento cuidadoso e atuação organizada para não gerar perda de tempo e 
código pouco legível e mantenível.

## Semelhanças entre as estruturas do `testthat` e de `gen_tests2`

* O padrão `testthat` tem um nível de aninhamento que é semelhante ao de 
  `gen_tests2`: `gen_tests2` tem seções, e `testthat` tem nomes de testes;
  
* O bloco de testes do `testthat` tem um conjunto de testes individuais; as 
  seções dos testes de `testthat` também;
  
* O bloco de testes do `testthat` tem início e fim marcado por chaves; os 
  testes de `testthat` têm `open.section()` e `close.section()`;

## Diferenças e problemas entre as estruturas do `testthat` e de `gen_tests2`

Segue uma listagem das diferenças entre `testthat` e `gen_tests2`:

* O padrão `testhat` exige funções semelhantes à `expect_equal()` que é 
  usada no exemplo a seguir, derivado de `test-fact.R`:
  
  ```R
  test_that("Larger values", {
    expect_equal(fact( 6), 720)
    expect_equal(fact( 7), 5040)
    expect_equal(fact( 8), 40320)
    expect_equal(fact( 9), 362880)
    expect_equal(fact(10), 3628800)
  })
  ```
  
  No caso dos exemplos em `run_gen_tests2.R` para `gen_tests2`, há outra 
  estrutura. Por exemplo: 
  
  ```R
  open.section("Basic tests")

  gen.simple.test('aa bb cc cc bb aa', linearize('aa bb cc cc bb aa'))
  gen.simple.test('aa bb cc', linearize('aa bb cc'))

  close.section("Basic tests")
  ```

  É muito fácil converter `open.section("Larger values")` na abertura do bloco 
  de testes. Basta que gere a linha
  
  ```R
  test_that("Larger values", {
  ```
  
  E naturalmente, `close.section("Larger values")` precisa apenas gerar a 
  chave de fechamento `}`
  
  Contudo, uma linha de teste é 

  ```R
  linear <- c('aa', 'bb', 'cc', 'cc', 'bb', 'aa')
  expected.am.Json <- '{"aa":{"bb":2},"bb":{"aa":2,"cc":2},"cc":{"bb":2,"cc":2}}'
  test.name <- 'aa bb cc cc bb aa'
  verbose <- FALSE
  flag.error <- FALSE
  result <- make.test.line(result, linear, expected.am.Json, test.name,
		report, verbose = verbose, flag.error = flag.error)
  ```
  
  Neste caso, `linear` é o parâmetro de entrada para a função sendo testada, e 
  `expected.am.Json` é o resultado esperado. Não há no esquema típico do 
  `testthat` lugar para `test.name` -- cada teste será localizado por sua 
  posição no bloco de testes -- , e nem para `verbose` ou `flag.error` 
  

* O programa `run_a2f_test2.R` -- contendo todos os testes de `gen_tests2` -- 
  é executável.   Também foi observado que um programa como `test-fact.R` é 
  executável. 
  
  Contudo, os resultados gerados por `test-fact.R` são mínimos. 
  
  Assim, além de um conjunto de testes similar a `test-fact.R` para testar a 
  biblioteca `a2f_test2.R`, será necessário um programa à parte como 
  `start_test.R`, que gerará todo um relatório de testes.
  
* Até aqui foi possível gerar, usando `testthat` um sumário dos blocos de 
  testes, com várias estatísticas de tempo (*real*, *system* e *user*) e 
  informações dos testes que falharam, que passaram e de avaliações de testes
  ignorados (ou *skipped*) e que foram abortados devido a erro.
  
  Não foi possível ainda decodificar o resultado individual de cada teste, 
  informado na coluna `result`, em um formato por decodificar.
  
  Contudo, é duvidoso que nessa estrutura seja possível colocar informações 
  tão verborrágicas como aquelas que serão geradas quando as variáveis 
  `verbose` ou `flag.error` estiverem habilitadas.
  
* Para `testthat` os arquivos das bibliotecas a serem testadas ficam sob um 
  diretório `R`, e os arquivos com scripts de testes ficam sob o diretório
  `tests/testthat`.
  
  Não há estrutura de diretório prevista para `gen_tests2`.

## *Roadmap* ou sequência de trabalho

1. Criar função `expect_equal_am()` para teste de matrizes de associação.
  
    Outras funções serão criadas para outras estruturas (como matriz de 
    frequência e matriz de porcentagens), pois exigem comparadores 
    especializados -- já disponíveis em `a2f_test2.R`
    
    Estas funções (e outras subordinadas a ela) serão parte do arquivo 
    `gen_tests2.R`;

2. Habilitar variáveis `verbose` e `flag.error` através de arquivo de 
  configuração geral `gen_tests2.conf` no diretório `gen_tests2` sob o 
  diretório `scripts`;

3. Criar diretório `debug` para saída de informações de teste, sob o diretório
    `gen_tests2`.
  
    Gerar arquivos de depuração a partir do nome dos arquivos de sob testes
    `a2f_tests2.R`, mas com informação de data e hora, para evitar superposição;

4. Gerar arquivo `test-a2f_test2.R` criando nomes de teste de acordo com o que 
  foi comentado em 
  **Semelhanças entre as estruturas do `testthat` e de `gen_tests2`**, a 
  partir de um arquivo de nível mais alto `run_gen_test2.R`
  
    A função `open.section()` deve verificar se não há seção aberta 
    anteriormente, e, do mesmo modo, `close.section()` deverá verificar se 
    está, de fato, fechando seção com o mesmo nome;

5. Os arquivos de testes gerados por `gen_tests2.R` devem poder ser executados
  tanto sob `testthat`, quanto sob `covr`;

6. Colocar arquivos de teste gerados sob o nome `test-a2f_test2.R` em diretório 
   `tests/testthat`;

7. Colocar `a2f_test2.R` em diretório `R`;

8. Gerar arquivo de testes `testthat_a2f_test2.R` em diretório `scripts` acima 
  de `R` e `tests` para disparar testes com `testthat`;
  
    O arquivo `testthat_a2f_test2.R` será gerado pela função `open.tests()` em 
    `run_gen_tests2.R`; 

9. Gerar arquivo de testes `covr_a2f_test2.R` em diretório `scripts` acima de 
  `R` e `tests para disparar avaliação de cobertura dos testes com `covr`;
  
    O arquivo `covr_a2f_test2.R` será gerado pela função `open.tests()` em 
    `run_gen_tests2.R`; 
  
10. Criar arquivo `skip.conf` que listará -- por nome -- os testes a serem 
  ignorados, ou *skipped*, mantido no diretório `gen_tests2`, sob o 
  diretório `scripts`;

11. Desenvolver ferramenta para ler arquivo de resultados e gerar `skip.conf`
  para os testes que passaram.
  
## Referências

* [gen_tests2]: https://youtu.be/LYFL2q5zE04 "https://youtu.be/LYFL2q5zE04"

  R ideas -- Matriz de associação em R 7
  
  [https://youtu.be/LYFL2q5zE04](https://youtu.be/LYFL2q5zE04)
  
  Vídeo documentando `gen_tests2`.
  
  Visitado em 05/07/2021

* [covr_link]: https://github.com/r-lib/covr "https://github.com/r-lib/covr"
  
  r-lib/covr: Test coverage reports for R
  
  [https://github.com/r-lib/covr](https://github.com/r-lib/covr)
  
  Visitado em 26/06/2021

