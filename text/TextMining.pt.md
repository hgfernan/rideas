# Mineração de textos, com o pacote `tm` do `R`

Um texto em língua natural também pode ser tratado pela estatística. Esse processo se chama mineração de textos, e tenta extrair propriedades numéricas nos textos.

É possível desenvolver softwares em `R` para fazer isso: a linguagem é flexível, poderosa e este será um exercício interessante para dominar seus recursos.

Contudo, `R` é também um ecossistema com muitos pacotes, que não apenas permitem ganhar tempo de desenvolvimento, mas também permitem aprender conceitos novos sobre os assuntos que abordam: os pacotes são em geral escritos por autores com experiência nos dados assuntos.

Assim, mesmo para quem quer aprender a programar em `R`, os pacotes podem fornecer temas interessantes para exercícios mais avançados, que tentarão emular, ou repetir, funcionalidades dos pacotes.

Além disso, a existência de pacotes permite que quem está querendo resolver um problema possa se focar na solução, em vez de focar na criação de ferramentas.

Neste vídeo vamos discutir todo o processo de preparar um texto para mineração de dados. Contudo, para evitar que o vídeo fique muito longo, deixaremos a mineração em si para o próximo vídeo da série.

## Baixar o arquivo

O `R` tem recursos relativamente completos para baixar arquivos; por exemplo, é possível passar-se uma URL para a função que baixa arquivos `download.file()`, mas a verdade é que a Internet (principalmente a *Web*) é uma floresta de protocolos e truques, que algumas vezes as funções do `R` não conseguem resolver.

Foi o caso de nosso exemplo: vamos baixar o texto de *A cartomante* de Machado de Assis de
[http://www.dominiopublico.gov.br/download/texto/bv000257.pdf](http://www.dominiopublico.gov.br/download/texto/bv000257.pdf), mas no meio do processo há uma redireção com código `302` e `download.file()` não sabe resolver.

Assim, foi necessário usar o recurso de baixar o arquivo pelo navegador. Poderia também ser usado um recurso mais especializado em internet, como `wget`.

É muito possível que haja outros recursos em `R` que façam isso, mas não vale a pena procurar, por ora.

Dois pontos importante:

1. As análises de mineração de textos costumam usar textos muito
    maiores do que as 7 ou 8 páginas de *A cartomante*, para obter as propriedades estatísticas de textos e autores.

    Por simplicidade e facilidade de tratamento, usaremos apenas um conto, não um romance ou, melhor ainda, toda a obra de Machado de Assis;

2. O ideal, para mineração de textos, seria ter o material em
    modo texto, ou `.txt`, para facilidade de manipulação. No entanto, *A cartomante* estava disponível apenas em formato PDF.

    Felizmente, esse é um problema já detectado há muito tempo e o pacote `tm` já traz solução para ele.

## Carregar (ou *importar*) pacotes

O pacote `tm` para mineração de texto não está disponível na instalação padrão do `R` e do `rstudio`. Assim, tem que ser instalado pela função `install.packages()`

Ela será apresentada em outra ocasião: apesar de simples, `install.packages()` exige algumas informações técnicas variáveis em diferentes sistemas operacionais.

Assim, suporemos que `tm` já está instalado, e apenas faremos sua *carga* ou *importação* no programa através de

```R
library('tm')
```

## Ler o arquivo PDF

Supõe-se que o arquivo PDF foi baixado no diretório `data`, que está no mesmo nível do arquivo dos programas. Ou seja: escreve-se `../data` para acessá-lo.

A leitura do arquivo PDF, já baixado, é feito em dois passos.

1. geração de uma *closure* (similar a uma função), que vai fazer
    a leitura apenas do texto;

    ```R
    pdfFunc <- readPDF(control = list(text = "-layout"))
    ```

2. acionamento da *função* identificada antes, informando-se o nome e localização do arquivo PDF, e a língua em que ele foi escrito;

    ```R
    txt <- pdfFunc(elem = list(uri = "../data/bv000257.pdf"), 
                   language = "pt")
    ```

## Transformação das estruturas em vetores

Tendo o arquivo PDF sido convertido em texto, trata-se agora de identificar a estrutura de dados que ele segue, e colher apenas o vetor de texto que deverá ser passado para seu processamento pelo pacote `tm`.

Isto é obtido pela função `str()`:

```R
> str(txt)
List of 2
 $ content: chr [1:7] "A Cartomante, Machado de Assis\nFonte:\nASSIS, Machado de. Obra Completa. Rio de Janeiro : Nova Aguilar 1994. v"| __truncated__ "— Qual saber! tive muita cautela, ao entrar na casa.\n— Onde é a casa?\n— Aqui perto, na Rua da Guarda Velha; n"| __truncated__ "mãe de Camilo, e nesse desastre, que o foi, os dois mostraram-se grandes\namigos dele. Vilela cuidou do enterro"| __truncated__ "era possível.\n— Bem, disse ela; eu levo os sobrescritos para comparar a letra com as das\ncartas que lá aparec"| __truncated__ ...
 $ meta   :List of 7
  ..$ author       : chr "EspartacoCoelho"
  ..$ datetimestamp: POSIXct[1:1], format: "2106-02-07 03:28:15"
  ..$ description  : NULL
  ..$ heading      : chr "Microsoft Word - acartomante.html"
  ..$ id           : chr "bv000257.pdf"
  ..$ language     : chr "pt"
  ..$ origin       : chr "PScript5.dll Version 5.2"
  ..- attr(*, "class")= chr "TextDocumentMeta"
 - attr(*, "class")= chr [1:2] "PlainTextDocument" "TextDocument"
```

Assim, estamos interessados no membro `content` da estrutura `txt`:

```R
text <- txt$content
```

## Eliminação de ruído do texto

Antes de realizar operações de mineração de texto, é necessário avaliar o conteúdo disponível: ele pode ter informações desnecessárias para essa tarefa.

A avaliação anterior mostrou que `text` é um vetor de caracteres, com 7 elementos. Sabemos pela visualização anterior, que o conto *A cartomante* tem 7 páginas. Assim, entendemos que os sete elementos de `text` são suas páginas.

Imprimindo a primeira, vemos:

```R
> text[1]
[1] "A Cartomante, Machado de Assis\nFonte:\nASSIS, Machado de. Obra Completa. Rio de Janeiro : Nova Aguilar 1994. v. II.\nTexto proveniente de:\nA Biblioteca Virtual do Estudante Brasileiro <http://www.bibvirt.futuro.usp.br>\nA Escola do Futuro da Universidade de São Paulo\nPermitido o uso apenas para fins educacionais.\nTexto-base digitalizado por:\nNúcleo de Pesquisas em Informática, Literatura e Lingüística\n(http://www.cce.ufsc.br/~nupill/literatura/literat.html)\nEste material pode ser redistribuído livremente, desde que não seja alterado, e que as\ninformações acima sejam mantidas. Para maiores informações, escreva para\n<bibvirt@futuro.usp.br>.\nEstamos em busca de patrocinadores e voluntários para nos ajudar a manter este projeto.\nSe você quer ajudar de alguma forma, mande um e-mail para <bibvirt@futuro.usp.br> e\nsaiba como isso é possível.\n                                        A Cartomante\n        HAMLET observa a Horácio que há mais cousas no céu e na terra do que\n        sonha a nossa filosofia. Era a mesma explicação que dava a bela Rita ao\n        moço Camilo, numa sexta-feira de novembro de 1869, quando este ria dela,\n        por ter ido na véspera consultar uma cartomante; a diferença é que o fazia\n        por outras palavras.\n        — Ria, ria. Os homens são assim; não acreditam em nada. Pois saiba que\n        fui, e que ela adivinhou o motivo da consulta, antes mesmo que eu lhe\n        dissesse o que era. Apenas começou a botar as cartas, disse-me: \"A senhora\n        gosta de uma pessoa...\" Confessei que sim, e então ela continuou a botar as\n        cartas, combinou-as, e no fim declarou-me que eu tinha medo de que você\n        me esquecesse, mas que não era verdade...\n        — Errou! interrompeu Camilo, rindo.\n        — Não diga isso, Camilo. Se você soubesse como eu tenho andado, por sua\n        causa. Você sabe; já lhe disse. Não ria de mim, não ria...\n           Camilo pegou-lhe nas mãos, e olhou para ela sério e fixo. Jurou que lhe\n        queria muito, que os seus sustos pareciam de criança; em todo o caso,\n        quando tivesse algum receio, a melhor cartomante era ele mesmo. Depois,\n        repreendeu-a; disse-lhe que era imprudente andar por essas casas. Vilela\n        podia sabê-lo, e depois...\n"
```

Assim, no cabeçalho da primeira página temos informações que descrevem a edição, mas que não fazem parte da obra. Como retirá-las ?

O mais simples será quebrar o texto por `strsplit()` segundo o caracter `'\n'`, que faz a quebra de linhas:

```R
lines <- strsplit(text[1], '\n')
```

Agora é necessário avaliar a estrutura de `lines`:

```R
> str(lines)
List of 1
 $ : chr [1:36] "A Cartomante, Machado de Assis" "Fonte:" "ASSIS, Machado de. Obra Completa. Rio de Janeiro : Nova Aguilar 1994. v. II." "Texto proveniente de:" ...
 ```

É uma lista, cujo único elemento é um vetor de 36 linhas. Como precisamos trabalhar com vetores (que estão no texto), é necessário transformar `lines` em um vetor de caracteres. Isto é simples:

```R
lines <- lines[[1]]
```

Agora, finalmente, falta avaliar a extensão do ruído. O modo tradicional em `R` é através de `head()`:

```R
> head(lines)
[1] "A Cartomante, Machado de Assis"                                                 
[2] "Fonte:"                                                                         
[3] "ASSIS, Machado de. Obra Completa. Rio de Janeiro : Nova Aguilar 1994. v. II."   
[4] "Texto proveniente de:"                                                          
[5] "A Biblioteca Virtual do Estudante Brasileiro <http://www.bibvirt.futuro.usp.br>"
[6] "A Escola do Futuro da Universidade de São Paulo" 
```

Infelizmente, não temos certeza se o cabeçalho termina na sexta linha. Por isso, é preciso visualizar mais de `lines`:

```R
> lines[1:10]
 [1] "A Cartomante, Machado de Assis"                                                 
 [2] "Fonte:"                                                                         
 [3] "ASSIS, Machado de. Obra Completa. Rio de Janeiro : Nova Aguilar 1994. v. II."   
 [4] "Texto proveniente de:"                                                          
 [5] "A Biblioteca Virtual do Estudante Brasileiro <http://www.bibvirt.futuro.usp.br>"
 [6] "A Escola do Futuro da Universidade de São Paulo"                                
 [7] "Permitido o uso apenas para fins educacionais."                                 
 [8] "Texto-base digitalizado por:"                                                   
 [9] "Núcleo de Pesquisas em Informática, Literatura e Lingüística"                   
[10] "(http://www.cce.ufsc.br/~nupill/literatura/literat.html)" 
```

Para visualizar as próximas 10 linhas:

```R
> lines[11:20]
 [1] "Este material pode ser redistribuído livremente, desde que não seja alterado, e que as" 
 [2] "informações acima sejam mantidas. Para maiores informações, escreva para"               
 [3] "<bibvirt@futuro.usp.br>."                                                               
 [4] "Estamos em busca de patrocinadores e voluntários para nos ajudar a manter este projeto."
 [5] "Se você quer ajudar de alguma forma, mande um e-mail para <bibvirt@futuro.usp.br> e"    
 [6] "saiba como isso é possível."                                                            
 [7] "                                        A Cartomante"                                   
 [8] "        HAMLET observa a Horácio que há mais cousas no céu e na terra do que"           
 [9] "        sonha a nossa filosofia. Era a mesma explicação que dava a bela Rita ao"        
[10] "        moço Camilo, numa sexta-feira de novembro de 1869, quando este ria dela,"
```

Assim, ficamos sabendo que as 16 primeiras linhas são desnecessárias nesta análise. Para eliminá-las, pode-se fazer:

```R
pag1 <- lines[17:length(lines)]
```

Agora falta reunir todas linhas restantes da página 1 em uma única cadeia de caracteres. Isto é feito por `paste()`, concatenando cada linha com o caracter de controle `'\n'`:

```R
pag1 <- paste(pag1, collapse = '\n')
```

Falta avaliar se a página original terminava com `'\n'` ou não:

```R
> print(text[1])
[1] "A Cartomante, Machado de Assis\nFonte:\nASSIS, Machado de. Obra Completa. Rio de Janeiro : Nova Aguilar 1994. v. II.\nTexto proveniente de:\nA Biblioteca Virtual do Estudante Brasileiro <http://www.bibvirt.futuro.usp.br>\nA Escola do Futuro da Universidade de São Paulo\nPermitido o uso apenas para fins educacionais.\nTexto-base digitalizado por:\nNúcleo de Pesquisas em Informática, Literatura e Lingüística\n(http://www.cce.ufsc.br/~nupill/literatura/literat.html)\nEste material pode ser redistribuído livremente, desde que não seja alterado, e que as\ninformações acima sejam mantidas. Para maiores informações, escreva para\n<bibvirt@futuro.usp.br>.\nEstamos em busca de patrocinadores e voluntários para nos ajudar a manter este projeto.\nSe você quer ajudar de alguma forma, mande um e-mail para <bibvirt@futuro.usp.br> e\nsaiba como isso é possível.\n                                        A Cartomante\n        HAMLET observa a Horácio que há mais cousas no céu e na terra do que\n        sonha a nossa filosofia. Era a mesma explicação que dava a bela Rita ao\n        moço Camilo, numa sexta-feira de novembro de 1869, quando este ria dela,\n        por ter ido na véspera consultar uma cartomante; a diferença é que o fazia\n        por outras palavras.\n        — Ria, ria. Os homens são assim; não acreditam em nada. Pois saiba que\n        fui, e que ela adivinhou o motivo da consulta, antes mesmo que eu lhe\n        dissesse o que era. Apenas começou a botar as cartas, disse-me: \"A senhora\n        gosta de uma pessoa...\" Confessei que sim, e então ela continuou a botar as\n        cartas, combinou-as, e no fim declarou-me que eu tinha medo de que você\n        me esquecesse, mas que não era verdade...\n        — Errou! interrompeu Camilo, rindo.\n        — Não diga isso, Camilo. Se você soubesse como eu tenho andado, por sua\n        causa. Você sabe; já lhe disse. Não ria de mim, não ria...\n           Camilo pegou-lhe nas mãos, e olhou para ela sério e fixo. Jurou que lhe\n        queria muito, que os seus sustos pareciam de criança; em todo o caso,\n        quando tivesse algum receio, a melhor cartomante era ele mesmo. Depois,\n        repreendeu-a; disse-lhe que era imprudente andar por essas casas. Vilela\n        podia sabê-lo, e depois...\n"
```

Ou seja, é necessário acrescentar um `'\n'` a `pag1`:

```R
pag1 <- paste(pag1, '\n')
```

O mais típico do estilo de programação em `R` seria fazer as duas operações de concatenação das linhas de `lines` em uma única expressão:

```R
pag1 <- paste(paste(lines[17:length(lines)], collapse = '\n'), '\n')
```

Mas, naturalmente, expressões mais longas podem ser mais difíceis de se entender e tornar corretas.

Agora, falta trocar o conteúdo original da página 1 pelo conteúdo sem o cabeçalho.

```R
text[1] <- pag1
```

## Carregando o documento processado em um *corpus*

Até aqui, o que fizemos foi permitir que o documento sem ruído pudesse ser processado. Agora, poderemos carregá-lo em um *corpus* -- ou coleção de textos do `tm`.

```R
cartomante <- VCorpus(VectorSource(text))
```

Nesta linha, `VCorpus()` cria o *corpus*, e `VectorSource()` informa que as informações serão carregadas de um vetor de caracteres. Haveria a possibilidade de criar o *corpus* a partir de um arquivo, ou até mesmo de um diretório de arquivos já preparados.

Poderíamos agora salvar o conteúdo em um arquivo, mas vamos realizar outros processamentos usando estruturas na memória.

## Algumas transformações

Há várias transformações possíveis em `tm`. Mas uma das primeiras é remover os espaços em branco. Em um texto pequeno como *A cartomante*, isto não é importante, mas poderá ser necessário em *corpus* maiores:

```R
cartomante <- tm_map(cartomante, stripWhitespace)
```

É importante observar que, neste caso, espaço em branco tem uma definição mais ampla: além do `' '`, espaço em branco entre dois caracteres, também são considerados *white space* os caracteres de tabulação `'\t'`, o de quebra de linha `'\n'` etc.

No entanto, assim o texto se torna mais fácil de ser analisado.

Outra transformação usual é tornar o texto em minúsculas:

```R
cartomante <- tm_map(cartomante, content_transformer(tolower))
```

Agora podemos, por exemplo, avaliar a frequência das palavras. Para isso, é necessário criar uma `DocumentTermMatrix` do *corpus* que está sendo usado:

```R
dtm <- DocumentTermMatrix(cartomante)
```

Ela foi criada com as opções *default* -- há muitas possibilidades de tratamento. E, finalmente pode-se inspecionar a matriz:

```R
> inspect(dtm)
<<DocumentTermMatrix (documents: 7, terms: 1336)>>
Non-/sparse entries: 1785/7567
Sparsity           : 81%
Maximal term length: 16
Weighting          : term frequency (tf)
Sample             :
    Terms
Docs camilo com ele era mas não para por que uma
   1      1   0   1   4   1   5    1   4  15   2
   2     11   2   4   4   4  13    4   3  18   4
   3      7   3   5   4   5   6    5   5  21   6
   4      6   8   4   7   3   4    4   2  13   3
   5      4   3   5   6   5   3    6   5  15   8
   6      7   7   4   2   0   5    3   3  13   4
   7      2   3   0   1   1   3    3   0   6   3
```

As estatísticas foram feitas sobre cada uma das 7 páginas do texto, não sobre o texto todo.

Como não há interesse em particular em cada uma das páginas do conto, vamos transformá-lo em um vetor de texto com um único elemento -- junção de todas páginas. Neste caso, não será necessário acrescentar `'\n'` em cada página, mas apenas avisar que todas páginas devem ser concatenadas.

```R
text.joined <- paste(text, collapse = '')
```

E então, repetir o processo de criar um *corpus*:

```R
> cartomante <- VCorpus(VectorSource(text.joined))
> cartomante <- tm_map(cartomante, content_transformer(tolower))
> dtm <- DocumentTermMatrix(cartomante)
> inspect(dtm)
<<DocumentTermMatrix (documents: 1, terms: 1336)>>
Non-/sparse entries: 1336/0
Sparsity           : 0%
Maximal term length: 16
Weighting          : term frequency (tf)
Sample             :
    Terms
Docs camilo com ele era mas não para por que uma
   1     38  26  23  28  19  39   26  22 101  30
```

Considera-se que palavras de ligação, como *com*, *mas* etc. não são significativas em muitas análises. Assim, uma prática comum é removê-las. O termo em português para elas é "palavras vazias", mas em inglês se diz *stopwords*:

```R
> cartomante <- tm_map(cartomante, removeWords, stopwords("portuguese"))
> dtm <- DocumentTermMatrix(cartomante)
> inspect(dtm)
<<DocumentTermMatrix (documents: 1, terms: 1230)>>
Non-/sparse entries: 1230/0
Sparsity           : 0%
Maximal term length: 16
Weighting          : term frequency (tf)
Sample             :
    Terms
Docs camilo camilo, cartas cartomante casa olhos rita rita, ser vilela
   1     38       7      8         11    9     8    9     7   9     16
```

Pode-se observar que foram incluídas na lista "palavras" formadas por uma palavra e um sinal de pontuação. Assim, faltou eliminar a pontuação do texto:

```R
> cartomante <- tm_map(cartomante, removePunctuation)
> dtm <- DocumentTermMatrix(cartomante)
> inspect(dtm)
<<DocumentTermMatrix (documents: 1, terms: 1014)>>
Non-/sparse entries: 1014/0
Sparsity           : 0%
Maximal term length: 15
Weighting          : term frequency (tf)
Sample             :
    Terms
Docs camilo cartas cartomante casa disse nada olhos rita rua vilela
   1     48     12         19   17    12   11    11   18  11     23
```

Outras estatísticas poderiam ser realizadas, mas fazem mais sentido apenas em *corpus* maiores do que este que incluiu apenas o conto *A cartomante*.
