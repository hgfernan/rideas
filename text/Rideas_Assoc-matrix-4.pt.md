---
output: pdf_document
---
# De `assoc.matrix` para matriz de frequência, funções auxiliares

## O que foi feito

Depois do vídeo anterior, foram feitas três tipos de alterações nos programas da série "Matriz de Associação":

1. Foi revisada a função principal `gen.assoc.matrix()` para que todas palavras sejam colocadas em único vetor linear.

    Com isto, a função passou em todos testes anteriores, e também passou em novos testes, nos quais números diferentes de palavras iguais eram processadas em linhas diferentes;

2. Foram criadas funções auxiliares que vão permitir expressar testes;

3. Foi criada a função `gen.freq.matrix()` que transforma a estrutura esparsa `assoc.matrix` em uma matriz densa de frequências.

4. Foi criada a função `gen.percent.matrix()` que transforma uma matriz retangular de frequências em uma matriz de porcentagens de associação -- afinal a associação de palavras que está sendo construída.

## Funções auxiliares

Foram criadas duas funções auxiliares para tratar com o tipo `assoc.matrix`, que serão úteis na etapa de testes a seguir.

### `valid.assoc.matrix()`

A ideia de `valid.assoc.matrix()` é testar se uma dada matriz de associação `am` tem todas características que se espera de uma matriz de associação:

1. deve ser uma lista não nula;

2. toda linha deve ter uma palavra associada a ela.
  
    Portanto, seu vetor de nomes para linhas não pode ser nulo;

3. Para cada uma das linhas, da lista externa da matriz de associação, são feitos os seguintes testes:

    1. verifica-se se a linha é nula;

    2. verifica-se se a linha é uma lista;

    3. verifica-se se a linha tem comprimento zero;

    4. verifica-se se a linha possui uma lista de nomes não nula;

    5. para cada uma das posições da linha, são feitos os testes:

        1. verifica-se se a posição é nula;

        2. verifica-se se a posição contém um número;

        3. obtém-se a linha correspondente ao nome da coluna.

            Testa-se se é uma lista não nula. Este teste é redundante nas últimas linhas, mas pode acelerar a
            detecção de posições inválidas nas primeiras linhas;

        4. testa-se a frequência da associação das palavras na linha e coluna são iguais àquela da associação de coluna e linha.

4. Todos testes anteriores interrompem a função e retornam `FALSE` caso indiquem erro. Portanto, se nenhum dos testes anteriores detectar erro, então a matriz de associação pode ser considerada válida.

### `equal.assoc.matrix()`

A função `equal.assoc.matrix()` será muito importante nos testes, verifica se duas matrizes de associação são iguais.

São realizados os seguintes passos:

1. Se os tipos dos parâmetros são diferentes, retorna `FALSE`;

2. Se o comprimento dos parâmetros são diferentes, retorna `FALSE`;

3. Se qualquer um dos parâmetros não é uma matriz de associação válida, retorna `FALSE`;

4. Se não são iguais os vetores de nomes ordenados dos parâmetros, retorna `FALSE`;

5. Para cada uma das linhas das matrizes:

    1. Se o comprimento das linhas for diferente, retorna `FALSE`;

    2. Se o valor de linha e coluna de uma delas for diferente da mesma posição da outra, retorna `FALSE`.

6. Caso contrário, não tendo parado em nenhum desses casos, retorna `TRUE`.
