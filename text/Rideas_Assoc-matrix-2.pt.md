# Testes para `gen.assoc.matriz()`

## Metodologia e problemas a resolver

No vídeo passado, discutimos sobre uma apresentação, sem abrir o
`rstudio` para mostrar código. Neste só vamos falar de código,sem abrir uma apresentação.

A proposta aqui é implementar testes, também conhecidos por "casos de teste", ou *test cases*. É preciso implementar alterações para favorecer o tratamento de muitos testes.

Além, é claro, de corrigir o código à medida que novos testes surgem.

No limite, pode não ser conveniente alterar o código para tratar de casos de teste que proponham situações muito fora da prática. Contudo, vale a ideia de *programming by contract* (ou "programação por contrato"): o código deve tentar atender uma especificação bem definida.

Em termos ideais, o código deveria ser capaz de reconhecer quando o contrato não está sendo cumprido. Isto é: quando o código recebe dados que não atendem às especificações que se propõe a atender. E, neste caso, reclamar devidamente.

## Função de impressão de matrizes de associação

Para imprimir vários testes, é conveniente ter uma rotina de impressão de matrizes de associação. Infelizmente, a impressão através de `print()` não traz bons resultados.

Considere as primeiras linhas do relatório de saída para apenas dois casos de teste:

```Rout
character(0)
list()

[1] "aa bb cc" "dd aa ee"
$aa
$aa$bb
[1] 1

$aa$dd
[1] 1

$aa$ee
[1] 1
```

A parte do relatório que comenta o uso de um vetor vazia não faz nenhum comentário a respeito de ter sido enviado um vetor inválido. Não há problema de se retornar uma lista vazia para esta solicitação, mas seria conveniente que esssa informação estivesse no relatório de saída.

Além disso, a impressão de um texto mínimo com 2 linhas, e 5 palavras se estende por muitas e muitas linhas, o que tornará muito pouco prático realizar testes mais longos e, portanto, mais próximos da realidade de um texto.

Para atender a esses dois requisitos foi proposta uma função de impressão de matrizes de associação:

```R
print.assoc.matrix <- function(am) {
  if (length(am) == 0) {
    cat('*** NULL association matrix received***\n')
    return(NULL)  
  }
  
  for (row in names(am)) {
    cat('"', row, '"\n', sep = '')
    row.list <- am[[row]]
    cat('  ', paste('"', names(row.list), '":', row.list, collapse = ' ', sep = ''), 
        '\n', sep = '')
  }
  
}
```

Para o caso do vetor caracter `c('aa', 'bb', 'cc')`, tem-se a impressão

```Rout
"aa"
  "bb":1
"bb"
  "aa":1 "cc":1
"cc"
  "bb":1
```

## Função de testes

O trecho original para testes era apenas uma repetição de comandos:

```R
cat('\n')
cv <- c('aa bb cc', 'dd aa ee')
print(cv)
res <- gen.assoc.matrix(cv)
print(res)
```

Para minimizar a repetição de código e aumentar a possibilidade de tornar a função mais importante, foi criada a função
`show.test.case()`, que mostra o vetor de caracteres usado e seu resultado.

```R
show.test.case <- function (char.vector) {
  cat('Character vector to analyse\n')
  
  if (length(char.vector) == 0) {
    cat('*** NULL character value received***\n\n')
    return(NULL)  
  }
  
  for (line in char.vector) {
    cat('  ', format(line), '\n', sep = '')
  }
  cat('\n')
  
  res <- gen.assoc.matrix(char.vector)
  
  cat('Resulting association matrix\n')
  print.assoc.matrix(res)
  cat('\n\n')
}
```

## Alteração do código para atender novos casos de teste

Com a maior capacidade de criação de novos testes e apresentação mais concisa da matriz de resultados, foi possível introduzir novos casos de teste.

Em particular, foram realizados testes com repetição de palavras, e repetição de palavras em várias linhas.

O código foi alterado para passar nestes testes.

## Próximos passos

1. Teste de tipo de dados nos parâmetros;

2. Gerador de matrizes de associação, a partir de cadeias de
    caracteres no estilo JSON;

3. Teste automático, em vez de visual, usando cadeias de caracteres criadas no passo 2;

4. Criação de classe `assoc.matrix`, com construtor a partir de vetor de caracteres e métodos para impressão (`print()`) e teste de igualdade entre matrizes.
