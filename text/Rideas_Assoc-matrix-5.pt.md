---
output: 
  pdf_document: 
    toc: yes
    keep_tex: yes
---
# Geração de testes automatizados

Nesta etapa do projeto matrizes de associação estão sendo gerados testes 
automáticos, que tentarão validar a função principal da biblioteca 
`assoc.matrix`.

Mesmo que não seja possível executar a infinidade de testes possíveis, ainda 
assim um conjunto suficientemente grande de testes, com alguns casos bem 
escolhidos permitirá identificar os problemas (ou *bugs*) mais imediatos da 
biblioteca e, assim, aumentar seu grau de confiabilidade.

## O que é um teste automatizado ?

Mesmo que, para pequenas quantidades de testes, seja viável e até mesmo 
recomendável que o resultado dos testes seja validado visualmente, por um
programador ou testador, para quantidades maiores de testes isto se torna 
demorado e, pela quantidade de operações necessárias, até mesmo pouco confiável. 

Assim, nestes casos é importante ter testes que, para parâmetros definidos, 
gerem um resultado, que é então comparado com um resultado esperado. 

Quando o resultado **gerado** é igual ou equivalente ao resultado **esperado**, 
então diz-se que o teste **passou** (ou *passed*); caso contrário, diz-se que o
teste *falhou**, ou *failed*.

Naturalmente, a dificuldade maior neste contexto está em gerar testes para os 
quais se conheça o resultado antecipadamente.

## Estratégia para teste automatizado de matriz de associação

Os testes automatizados envolvem a geração de uma matriz de associação 
para uma dada sequência linear de palavras em um vetor de caracteres, comparada 
com a matriz de associação esperada para ela.

Faremos uma massa de testes de dois tipos: 

1. testes **simples**, que podem ter seu resultado esperado fornecido por mera 
  inspeção da cadeia de caracteres de entrada.
  
    É bem verdade que se espera maior número de falhas em testes mais extensos, 
    porque exigem mais das funções sendo avaliadas. Além disso, as situações de 
    uso real envolvem tipicamente uma quantidade grande de operações, que se 
    pode imitar com testes extensos. 
  
    Assim, uma biblioteca será melhor testada por testes extensos. Porém, o 
    mínimo que se espera de uma biblioteca é que passe por testes triviais;
  
2. testes **aleatórios**, com cadeias de teste geradas aleatoriamente.

    Cadeias de teste serão geradas através de geradores de números 
    pseudo-aleatórios. 
  
Nos dois tipos de testes, as sequência linear de palavras serão acompanhadas de 
estimativas de suas matrizes de associação geradas por algoritmo diferente 
daquele da função `gen.assoc.matrix()`. Esse algoritmo é criado usando o 
conhecimento anterior da sequência linear de palavras.

## Bibliotecas em `R`

A partir desse momento as bibliotecas de funções -- tanto aquela de associação 
de palavras como aquela de testes deverão sofrer pouca ou nenhuma alteração. 

Por outro lado, os programas que as usam sofrerão muitas alterações, pois são 
eles que geram testes e os aplicam.

Assim, compensa separar biblioteca e programa que a usa em dois arquivos 
distintos.

No entanto, em `R`, diferente de outras linguagens interpretadas, é necessário
carregar a biblioteca antes de sua execução. Isto é feito através de um 
programa principal que, neste caso, tem o prefixo `run_`. 

Assim, a biblioteca que cria matrizes de teste e é chamada `gen_tests.R`, tem
um programa que a usa chamado `run_gen_tests.R`

Por sua vez, a biblioteca `a2f_tests.R`, que calcula matrizes de associação, 
terá um programa que a usa chamado `run_a2f_tests.R` 

Ele contém dezenas -- e em breve centenas -- de testes que 
exercitam a função `gen_assoc_matrix.R`

Deve-se notar que o programa `run_a2f_tests.R` é gerado pelo programa 
`run_gen_tests.R`

## A função `compare.test()`

A função `compare.test()` é responsável pela execução de testes.  Ela recebe
como parâmetros 

* `linear` que é um vetor de caracteres que contém palavras, cada uma delas 
    em uma posição diferente do vetor;

* `expected.am.Json`, uma matriz de associação transformada em uma cadeia de 
  caracteres em formato JSON;
  
* `verbose` é um valor lógico que indica se todas entradas e resultados devem 
  ser impressos. O valor *default* do parâmetro é `FALSE`;

* `flag.error` é um valor lógico que indica que, caso haja um erro, todos os 
  detalhes necessários para sua solução devem ser impressos. O valor 
  *default* do parâmetro é `FALSE`.
  
A operação da função é muito simples: ela recebe como parâmetro a sequência de 
palavras em `linear`, gera uma matriz de associação para ela, e a compara com
a matriz `expected.am.Json` -- que antes foi convertida do formato JSON para 
o formato usual para matrizes de associação. 

Caso a matriz gerada e aquela esperada sejam iguais -- comparadas através 
de `equal.assoc.matrix()`, o resultado retornado por `compare.test()` é 
`TRUE`, o que indica que o teste passou. Caso contrário, o resultado `FALSE`
indica que o teste falhou.

## O programa `run_gen_tests.R` e a biblioteca `gen_tests.R`

O programa `run_gen_tests.R` tem como função criar o programa 
`run_a2f_test.R`

Sua arquitetura é muito simples: ele é iniciado pela carga da biblioteca 
`gen_tests.R` e por chamadas da função `gen.test.line()`, que espera um vetor 
de caracteres, com uma palavra por posição.

Para evitar o custo sintático de separar palavras entre aspas, em geral 
`gen.test.line()` recebe como parâmetro a função `linearize()`, também da 
biblioteca `gen_tests.R`, que quebrará a cadeia de caracteres recebida em
palavras.

A função `gen.test.line()` é relativamente autônoma: além de gerar chamadas 
a `compare.test()`, ela também verifica se já foi gerada a carga do arquivo
de biblioteca de `run_a2f_test.R`

Isto simplifica muito a escrita de `run_gen_tests.R`, que é basicamente uma 
lista de  chamadas a `gen.test.line()`; ou seja: uma lista dos testes a serem 
gerados em `run_a2f_test.R`

Em maior detalhe, eis a estrutura do programa `run_gen_tests.R`: 

1. Carga da biblioteca `gen_test.R`;

2. Execução da função `create.header()`, que irá criar toda a inicialização do 
    teste;
    
3. Bloco de testes, criados com a função `gen.test.line()`, que recebe um 
    vetor de caracteres, com uma palavra por posição -- chamado no contexto 
    do programa de vetor *linear*.
    
    Nos testes simples, é informada apenas cadeia de caracteres, que a função
    `linearize()` transformará em um vetor linear.
    
    Nos testes mais avançados, são passados para o programa funções como 
    `gen.word.pair()`, capazes de gerar vetores lineares de modo 
    pseudo-aleatório;

4. Fechamento de testes, através da função `close.tests()`, que gera um
    bloco de comandos que mostra os totais e fecha o arquivo de saída.
    

## O programa `run_a2f_test.R` e a biblioteca `a2f_test.R`

A biblioteca `a2f_test.R` é praticamente a mesma definida no programa 
`am2freq.R` discutido em vídeo anterior. A biblioteca `a2f_test.R` só 
apresenta duas alterações em relação à versão anterior: 

1. inclusão da função `compare.test()` -- o que levou inclusive ao sufixo 
    `_test` no nome de `a2f_test.R`;

2. a retirada do programa principal, que agora é responsabilidade de 
    `run_a2f_test.R`
    
O fato do programa principal estar desacoplado da biblioteca é o que permite 
a geração autônoma de testes por `run_a2f_test.R`

O programa `run_a2f_test.R` tem a seguinte estrutura:

1. carga da biblioteca `a2f_test.R` através da função `source()` do `R`;

2. remoção do arquivo `run_gen_tests.Rout`, se houver;

3. abertura de um novo arquivo `run_gen_tests.Rout`;

4. inicialização do contadores de testes: `total.tests`, `passed.tests` e 
    `failed_tests`;

5. Enumeração dos testes a serem executados, através de um bloco que repete a 
    cada teste:
    
    1. atribuição da variável `linear`, que contém o vetor de caracteres a ser 
        processado;
        
    2. atribuição da variável `expected.am.Json`, que contém a matriz de 
        associação esperada para o vetor de caracteres `linear`;
        
    3. atribuição da variável `verbose` como `FALSE`. Este já é o valor 
        *default* do parâmetro da função `compare.test()`, mas é mencioado
        explicitamente para permitir a edição manual do programa 
        `run_a2f_test.R`, para detalhar algum teste, quer tenha passado ou
        falhado;
        
    4. atribuição da variável `flag.error` como `FALSE`. Este já é o valor 
        *default* do parâmetro da função `compare.test()`, mas é mencioado
        explicitamente para permitir a edição manual do programa 
        `run_a2f_test.R`, para detalhar algum teste, quer tenha falhado;
        
6. Fechamento do arquivo `run_gen_tests.Rout`, para garantir que todo seu 
    conteúdo seja gravado em disco.