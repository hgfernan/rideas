# `Eps`, ou avaliação de precisão

Esta é a primeira edição de uma nova série de textos, código e vídeos sobre ideias e implementações usando **scripts** do `R`.

Ela será executada em paralelo com a série **R para programadores**, que está construindo paulatinamente um conjunto de experiências e conceitos que formarão programadores em `R`.

Aqui serão discutidos temas de programação `R` que poderão, no futuro, fazer parte da série **R para programadores**, ou que, ao contrário, foram usados como base para criar vídeos para aquela série.

Também aqui poderão ser explorados pontos que foram sugeridos por apresentações feitas para outros públicos, como aquelas para a iniciativa [Inteligência Competitiva e de Mercado](https://www.youtube.com/channel/UCbw-mb10RLfs61LHJ5-Wxyg).

Neste primeiro vídeo será discutida uma generalização do conceito de *epsilon*, ou apenas *eps*, que é uma estimativa da precisão de um computador. Normalmente, ele é baseado na ordem de grandeza de `1`. Sua definição é "o maior número tal que, somado a `1` ainda gera o resultado `1`". Esta pode parecer uma formulação estranha, ou mesmo absurda. Mas se lembramos que é necessariamente finita a precisão dos números em ponto flutuante, fica fácil ver que o que se procura é o limite dessa precisão.

Um cálculo logarítimico simples permite calcular o numero de **dígitos significativos** que a representação de ponto flutuante permite para esta ordem de grandeza.

A proposta aqui é fazer uma estimativa do número de dígitos que o R permite para cada ordem de grandeza.

O método mais convencional de se calcular o `eps` é simplesmente, em `R`:

```R
eps = 1
while ((eps + 1) != 1) {
    eps = eps / 2
}
```

Isto, em uma versão 4.0.3 de 64 bits do `R` equivale a
`1.110223e-16`

Para calcular o número de dígitos significativos que isso permite, basta calcular o logaritmo na base de 10 de `eps`:

```R
significative.digits <- -trunc(log10(eps))
```

O que corresponde a 15.

Naturalmente, é negativo o logaritmo na base 10 de um número positivo menor do que um -- por isso o sinal de menos. A truncagem do número para inteiro é que precisamos contar as casas decimais de fato disponíveis, não frações delas.

Para generalizar esta estimativa para um número maior de que um, podemos usar uma variável `base`. Por exemplo, como no trecho a seguir:

```R
base = 10
eps = base
while ((eps + base) != base) {
    eps = eps / 2
}
```

Neste caso, `eps` é igual a `5.551115e-16`. E, para calcular o número de dígitos significativos é preciso dividir por base:

```R
significative.digits <- -trunc(log10(eps / base))
```

É claro que, pela propriedade dos logaritmos, poderíamos trocar a divisão por uma subtração, mas no mundo dos números aproximados nos computadores, a subtração deve ser evitada, porque pode levar a perder dígitos significativos.

Definida a teoria matemática simples por traz da estimativa do número de dígitos significativos disponível em uma ordem de grandeza, podemos criar um programa em `R` para calcular `eps` para várias ordens de grandeza.

Isto foi feito no programa `find_eps.R`, disponível em
[https://gitlab.com/hgfernan/rideas](https://gitlab.com/hgfernan/rideas)

Vamos apresentá-lo aos poucos. Começando pelas funções

```R
calc.eps <- function(base) {
  eps <- base
  while ((base + eps) != base) {
   eps <- eps / 2
  }
  
  eps
}

calc.significant.digits <- function (base, eps) {
  significant.digits <- - trunc(log10(eps / base))
  
  as.integer(significant.digits) 
}
```

A função `calc.eps()` faz exatamente os cálculos comentados aqui. Apenas lembra-se que o `eps` isolado em sua última linha indica o que a função deve retornar.

A função `calc.significant.digits()` calcula os dígitos significativos disponíveis para um valor de precisão `eps` em uma dada ordem de grandeza `base`.

A função `as.integer()` na última linha da função lembra que esse valor será retornado como um número inteiro, não como um valor em ponto flutuante.

O programa trabalha com ordens de grandeza simétricas em torno de `1`. Ou seja: caso se peça o cálculo de `3` ordens de grandeza, serão calculadas ordens de grandeza `10e-1 == 0,1`, `10e0 == 1` e `10e+1 == 10`.

Isto é dado pela variável `n.magnitudes`, que é definida no programa logo após as funções.

Como o número de ordens de grandeza é simétrico em torno de `1`, ele deve ser ímpar. Por isso a linha

```R
n.magnitudes <- n.magnitudes + (! as.logical(n.magnitudes %% 2))
```

Ela garante que, se o número de ordens de magnitude definido for par, ele será levado para o ímpar seguinte. Assim, como no início do programa ele foi definido como `22`, esta linha garantirá que ele passe a ser `23`, para garantir que `11` ordens de grandeza maiores do que `1` serão usadas, e também `11` ordens de grandeza maiores do `1`.

As linhas seguintes alocam três vetores para a `base` ou ordem de grandeza do cálculo, `eps` para o maior número que não é significativo nessa ordem de grandeza, e `digits` para o número de dígitos significativos.

```R
base <- double(n.magnitudes)
eps <- double(n.magnitudes)
n.digits <- integer(n.magnitudes)
```

Observe que os dois primeiros são vetores de dupla precisão, e o último é um vetor de inteiros. Todos têm o mesmo número de elementos, que é a quantidade de ordens de grandeza para as quais se deseja calcular.

O próximo conjunto de três linhas define o índice `mid.ind` da ordem de grandeza intermediária, e os índices iniciais para as ordens de grandeza superiores `upper.ind` e inferiores `lower.ind`.

```R
mid.ind <- (n.magnitudes %/% 2L) + 1L
upper.ind <- mid.ind + 1
lower.ind <- mid.ind - 1
```

O cálculo de `mid.ind` garante que será usado o índice que está no meio das ordens de grandeza. Por exempo, para 23, isto significa `(23L %/% 2L) + 1L == 11L + 1L == 12L`. Assim, haverá `11` ordens de grandeza inferiores a `1`, e `11` ordens de grandeza superiores a ele.

O operador `%/%` faz a divisão inteira -- sem números fracionários -- e os sufixos `L` garantem que estes são números inteiros.

As três linhas seguintes definem os valores de `base`, `eps` e `digits` para a ordem de grandeza `1`.

```R
base[mid.ind] <- 1
eps[mid.ind] <- calc.eps(base[mid.ind])
n.digits[mid.ind] <- calc.significant.digits(base[mid.ind], eps[mid.ind])
```

Os cáculos para as outras ordens de grandeza serão realizados no laço (ou *loop*) a seguir:

```R
while (lower.ind >= 1) {
  base[upper.ind] <- base[upper.ind - 1] * 10
  base[lower.ind] <- base[lower.ind + 1] / 10

  eps[upper.ind] <- calc.eps(base[upper.ind])
  eps[lower.ind] <- calc.eps(base[lower.ind])

  n.digits[upper.ind] <- calc.significant.digits(base[upper.ind], eps[upper.ind])
  n.digits[lower.ind] <- calc.significant.digits(base[lower.ind], eps[lower.ind])
  
  upper.ind <- upper.ind + 1
  lower.ind <- lower.ind - 1
}
```

O laço `while` é controlado por `lower.ind`, que não poderá ser menor do que `1`.

As duas linhas para o cálculo das ordens de grandeza usam o fato de que, a cada iteração, a ordem de grandeza superior é sua predecessora multipllicada `10`, e a ordem de grandeza inferior é sua predecessora dividida por `10`.

Por exemplo, na primeira iteração, a ordem de grandeza superior é `1 * 10 == 10`, e a ordem de grandeza inferior é `1 / 10 == 0,1`.

Os `eps` dessas ordens de grandeza são calculados a partir dos valores armazenados nas respectivas posições de `base`, chamando-se a função `calc.eps()`.

E, do mesmo modo, o número de dígitos significativos são estimados a partir dos valores de `eps`, usando-se para isso a função `calc.significant.digits()` para os valores de `eps` calculados antes.

Finalmente, as duas últimas linhas do laço `while` ajustam os índices das ordens de grandeza superior `upper.ind` e inferior `lower.ind` para a próxima iteração.

O laço `while` termina quando `lower.ind` é menor do que `1`.

O resultado é apresentado como relatório nas linhas seguintes. A linha

```R
cat("Base\tEps\t\tDigits\n")
```

imprime as palavras `Base`, `Eps` e `Digits` separadas pelo caracter de tabulação `\t`. São usados dois `\t` para separar `Eps` de `Digits` porque sob `Eps` são imprimidos números maiores.

As linhas do relatório para os valores de precisão são impressas por

```R
cat(paste(format(base, scientific = TRUE, digits = 3), 
          format(eps, scientific = TRUE, digits = 7), 
          format(n.digits, digits = 7), '\n', sep = '\t'
         ),
    sep = ''
   )
```

Linhas como esta são comuns nos programas escritos em `R`, e devemos nos habituar a compreendê-las. Mas, para facilitar sua compreensão, podemos vê-las de modo menos compacto, em variáveis que registrarão parte das operações com cadeias de caracteres.

As três linhas a seguir formatam os três vetores calculados:

```R
chr_base <- format(base, scientific = TRUE, digits = 3)
chr_eps <- format(eps, scientific = TRUE, digits = 7)
chr_n.digits <- format(n.digits, digits = 3)
```

É usada a função `format()` que representará os dados nos formatos desejados. Na primeira linha deste grupo, todos números em `base` são convertidos em cadeias de caracteres e armazenados em `chr_base`. O parâmetro `scientific = TRUE` informa que se deseja a notação científica. O parâmetro `digits` informa qual o número de dígitos significativos a ser usado.

A linha

```R
line <- paste(chr_base, chr_eps, n.chr_digits, '\n', sep = '\t')
```

gera cada linha do relatório, através da função `paste()`. Ela combina cada elemento dos vetores formatados. Eles são separados pelo tabulador `\t`, e cada uma dessas linhas é terminada pelo caracter de quebra de linha `\n`.

As linhas finais imprimem o relatório

```R
cat("Base\tEps\t\tDigits\n")
cat(line, sep = '')
```

Na nova versão do relatório, o mesmo cabeçalho é usado. E, finalmente, fica claro que `cat()` vai imprimir cada linha do relatório, armazenada em `line`, mas não vai adicionar nenhum separador às novas linhas.
