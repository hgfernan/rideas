#! /bin/bash

# OBS time is for bash only ?!? 

if [ $# -lt 1 ]; then
    echo "$0: ERROR -- input file was not informed"
    exit 1
fi
 
INP_NAME=$1

# TODO test if file exists

ORIG_FILE=temp/"${INP_NAME}"
DEST_FILE=${INP_NAME}

time ffmpeg -i ${ORIG_FILE} -vcodec libx264 -crf 23 ${DEST_FILE} 
