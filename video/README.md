# Video elements and results

| Video | File                             | Description                                              | Sources |
|------:|:---------------------------------|:---------------------------------------------------------|--------:|
|     1 | `Rideas-eps.pt.mp4`              |
|     2 | `Rideas-AnovaGrupo.pt.mp4`       |
|     3 | `Rideas-TextMining_tm.pt.mp4`    |
|     4 | `Rideas-TextMining_mac.pt-1.mp4` |
|     5 |