\documentclass{beamer}

\usepackage{latexsym}
\usepackage[brazilian]{babel}
\usepackage{lmodern}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{longtable}
%\usepackage[dvips]{graphicx}
\usepackage{graphicx}

%\usepackage[pdftex]{hyperref}
\usepackage{hyperref}
\usepackage{amsmath}  % for equation environment

\usepackage{listings}

\usepackage{textcomp} % to avoid warning of undefined font of code

\usetheme{default}

\definecolor{links}{HTML}{2A1B81} 
\hypersetup{colorlinks,urlcolor=links} % to paint links

\author{Hilton Fernandes}
\title{{\em Short circuiting} de operadores lógicos do {\tt R} ?\\
       {\sl R ideas} 
      }
      
\lstset{language = R,%
        basicstyle = \tiny,%
        showstringspaces = false,%
        numbers=left,%
        frame = single,%
        framexleftmargin=5mm,%
        numberstyle = \tiny,%
        extendedchars = true,%
        literate={á}{{\'a}}1 {ã}{{\~a}}1 {é}{{\'e}}1 {ó}{{\'o}}1
       }
       
\renewcommand{\lstlistingname}{Listagem}% Listing -> Algorithm
\renewcommand{\lstlistlistingname}{Lista de \lstlistingname{s}}% List of Listings -> List of Algorithms

\begin{document}

\begin{frame}
    \maketitle
\end{frame}

\begin{frame}{Agenda}
    \begin{enumerate}
        \item Motivação
        \item O que é {\em short circuiting} de operadores lógicos ?
        \item Os operadores lógicos principais do {\tt R}
        \item Execução do programa {\tt short\_circuit.R}
        \item Interpretação dos resultados
        \item Referências
    \end{enumerate}

\end{frame}


\begin{frame}{Motivação}
    \begin{itemize}
        \item Foi encontrado um pequeno {\em bug} inofensivo do {\tt R}
            que gerava avisos inesperados, em uma condição.
        \item Não tinha maiores consequências, mas trouxe dúvidas sobre a 
            ordem de avaliação de expressões no {\tt R}.
        \item Uma boa discussão sobre o tema foi encontrada no 
            \href{https://stackoverflow.com/}{Stack Overflow} em 
            \cite{rBooleanOperators}.
        \item A partir dela e de outras relevantes, foi desenvolvido o
            programa {\tt short\_circuit.R}, para avaliar esse tema, 
            importante para programação em {\tt R}.
    \end{itemize}

\end{frame}


\begin{frame}{Motivação ({\em cont.})}
    \begin{itemize}
        \item Na {\em playlist} do YouTube 
            \href{https://www.youtube.com/playlist?list=PLdgRds52HXiq-YlpmnxSPV9QgHEX5ggJ3}
                 {R para programar}, 
            o vídeo 
            \href{https://youtu.be/UMts8qhAWX8}
                 {R para programar, cap. 2 - Expressões lógicas no R 3/6}                 
            discute os operadores lógicos vetorizados {\tt \&} e 
            {\tt |}
            
        \item E também na série {\em R para programar}, o vídeo 
            \href{https://youtu.be/iDOr8Fcnf2A}
                 {R para programar, cap. 2 - Expressões lógicas no R 4/6}                 
            discute os operadores lógicos ``longos'' {\tt \&\&} e 
            {\tt ||}            
    \end{itemize}

\end{frame}


\begin{frame}{O que é {\em short circuiting} de operadores lógicos ?}
    \begin{itemize}
        \item Várias linguagens de programação não avaliam sempre toda 
            uma expressão lógica, mas só o suficiente para que ela possa
            ser decidida.
            
        \item Por exemplo, para avaliar {\tt (TRUE || func())}, 
            não será preciso chamar {\tt func()}, pois o valor {\tt TRUE} 
            já deu o resultado da condição, que é verdadeiro, independente
            do resultado de {\tt func()}.
        \item E para avaliar {\tt (FALSE \&\& func())}, não será preciso 
            chamar {\tt func()}, pois o valor {\tt FALSE} 
            já deu o resultado da condição, que é falso, independente
            do resultado de {\tt func()}.
    \end{itemize}

\end{frame}


\begin{frame}{O que é {\em short circuiting} de operadores lógicos ?
    ({\em cont.})}
    \begin{itemize}
        \item Isto é chamado de {\em short circuiting}, ou de 
            ``curto circuito'', por analogia com os circuitos elétricos,
            onde a eletricidade sempre busca o caminho mais curto.
    \end{itemize}

\end{frame}

\begin{frame}{O que é {\em short circuiting} de operadores lógicos ?
    ({\em cont.})}
    \begin{itemize}
        \item Isto é muito útil.
        \item Por exemplo, se a segunda parte da operação lógica for 
            demorada, resolver na primeira parte representa um ganho 
            de tempo.
        \item E o {\em short circuiting} pode ser usado para evitar 
            erros.
        \item Por exemplo, no trecho
        
            {\tt (x >= 0) \&\& (sqrt(x) > 2)}, 
            
            a segunda parte,
            que calcula a raiz quadrada de {\tt x}, só será executada 
            se {\tt x} for não negativo.
    \end{itemize}

\end{frame}


\begin{frame}{Os operadores lógicos principais do {\tt R}}
    \begin{enumerate}
        \item {\bf Negação}, {\em nega}, ou ``troca'' o valor lógico a que
            foi aplicada.
            
            Em {\tt R} é representada por {\tt !}
            
            Por exemplo:
            {\tt !FALSE} se torna {\tt TRUE} e vice-versa;
            
        \item {\bf conjunção}, que gera resultado
            verdadeiro quando seus dois operandos são verdadeiros.
            
            É representada por {\tt \&} e {\tt \&\&} ;
            
        \item {\bf disjunção}, que gera resultado
            verdadeiro quando pelo menos um de seus dois operandos é verdadeiro.
            
            É representada por {\tt |} e {\tt ||} .
        
    \end{enumerate}

\end{frame}


\begin{frame}{Os operadores lógicos principais do {\tt R} ({\em cont.})}
    \begin{itemize}
        \item Também está disponível no pacote base do {\tt R} a operação 
            {\bf disjunção exclusiva}, que só gera resultado verdadeiro
            quando um único operando for verdadeiro, não os dois.
            
        \item Mas não é usada amplamente em {\tt R} e muitos tutoriais 
            sobre operadores lógicos não a mencionam.
            
        \item No {\tt R}, a disjunção exclusiva, ou ``ou exclusivo'' é 
            representado por {\tt xor()}.
            
    \end{itemize}

\end{frame}


\begin{frame}{Os operadores lógicos principais do {\tt R} ({\em cont.})}
    \begin{itemize}
        \item Diz-se que os operadores {\tt \&} e {\tt |} são 
            ``vetorizados'': quando aplicados a dois vetores lógicos, retornam um vetor com o mesmo número de elementos.
        \item Diz-se que os operadores {\tt \&\&} e {\tt ||} não são 
            ``vetorizados'': quando aplicados a dois vetores lógicos, 
            examinam apenas a primeira posição de cada um deles, e 
            retornam um único valor lógico.            
    \end{itemize}

\end{frame}


\begin{frame}{Execução do programa {\tt short\_circuit.R}}
    \begin{itemize}
        \item As variáveis {\tt lFalse} e {\tt lTrue} e a função {\tt f()}
            serão usadas para avaliar se operadores lógicos do {\tt R}
            oferecem {\em short-circuiting} ou não.
    \end{itemize}    
    
    \lstinputlisting[caption = Criação de variáveis {\tt lFalse} e 
        {\tt lTrue} e da função {\tt f()} 
        em {\tt short\_circuit.R}, 
        firstline=1,lastline=7,firstnumber=1]
        {../script/short_circuit.R}
    
\end{frame}


\begin{frame}{Execução do programa {\tt short\_circuit.R}
    ({\em cont.})}
    \begin{itemize}
        \item Nas linhas 9 e 10 é feita apresentação de títulos;
        \item Na linha 11 o vetor com todos elementos {\tt FALSE} é 
            combinado, por conjunção vetorizada, com um vetor de
            chamadas de {\tt f()}.
    \end{itemize}    
    
    \lstinputlisting[caption = Teste de {\em short circuiting} usando 
        operador {\tt \&}, 
        firstline=9,lastline=11,firstnumber=9]
        {../script/short_circuit.R}
    
\end{frame}


\begin{frame}{Execução do programa {\tt short\_circuit.R}
    ({\em cont.})}
    \begin{itemize}
        \item Na linha 13 é feita apresentação de títulos;
        \item Na linha 14 o vetor com todos elementos {\tt FALSE} é 
            combinado, por conjunção longa, com um vetor de
            chamadas de {\tt f()}.
    \end{itemize}    
    
    \lstinputlisting[caption = Teste de {\em short circuiting} usando 
        operador {\tt \&\&}, 
        firstline=13,lastline=14,firstnumber=13]
        {../script/short_circuit.R}
    
\end{frame}


\begin{frame}{Execução do programa {\tt short\_circuit.R}
    ({\em cont.})}
    \begin{itemize}
        \item A disjunção com {\tt \&} fez executar 
            {\bf todas} chamadas de {\tt f()}.
            
            $\Rightarrow$ não houve {\em short circuiting}.
            
        \item A disjunção com {\tt \&\&} não fez executar 
            {\bf nenhuma} chamada de {\tt f()}.
            
            $\Rightarrow$ houve {\em short circuiting}.
    \end{itemize}    
    
    \lstinputlisting[caption = Execução de {\tt short\_circuit}
        para vetores,
        firstline=1,lastline=9,firstnumber=1]
        {../script/short_circuit.Rout}
    
\end{frame}


\begin{frame}{Execução do programa {\tt short\_circuit.R}
    ({\em cont.})}
    \begin{itemize}
        \item Nas linhas 16 e 17 é feita apresentação de títulos;
        \item Na linha 18 {\tt FALSE} é combinado, usando 
            {\tt \&}, com uma chamada de {\tt f()}.
    \end{itemize}

    \lstinputlisting[caption = Teste de {\em short circuiting} usando 
        operador {\tt \&},
        firstline=16,lastline=18,firstnumber=16]
        {../script/short_circuit.R}

\end{frame}


\begin{frame}{Execução do programa {\tt short\_circuit.R}
    ({\em cont.})}
    \begin{itemize}
        \item Nas linhas 20 é feita apresentação de títulos;
        \item Na linha 21 {\tt FALSE} é combinado, usando 
            {\tt \&\&}, com uma chamada de {\tt f()}.
    \end{itemize}    
    
    \lstinputlisting[caption = Teste de {\em short circuiting} usando 
        operador {\tt \&\&}, 
        firstline=20,lastline=21,firstnumber=20]
        {../script/short_circuit.R}
    
\end{frame}


\begin{frame}{Execução do programa {\tt short\_circuit.R}
    ({\em cont.})}
    \begin{itemize}
        \item Pode-se ver que, mesmo com um único valor lógico 
            {\tt FALSE}, o operador {\tt \&} não permitiu 
            {\em short circuit} e {\tt f()} foi chamada.
            
        \item Por outro lado, {\tt \&\&} mais uma vez fez 
            {\em short circuiting}, não tendo chamado {\tt f()}.
    \end{itemize}
    
    \lstinputlisting[caption = Execução de {\tt short\_circuit}
        para vetores unitários, 
        firstline=11,lastline=17,firstnumber=11]
        {../script/short_circuit.Rout}
    
\end{frame}


\begin{frame}{Execução do programa {\tt short\_circuit.R}
    ({\em cont.})}
    \begin{itemize}
        \item Nas linhas seguintes, o programa 
            {\tt short\_circuit} faz, para os operadores {\tt |} e 
            {\tt ||} as mesmas análises que foram aplicadas até aqui
            a {\tt \&} e {\tt \&\&}.
            
        \item A respeito de sua execução, vale repetir os livros 
            de matemática estadunidenses antigos: 
            
            {\em This is left to the reader as an exercise}.
            
            Ou seja: faça você mesmo esta parte\ldots{}
            
    \end{itemize}

\end{frame}


\begin{frame}{Interpretação dos resultados}
    \begin{itemize}
        \item A princípio, em linguagens de programação convencionais, 
            uma linha como 
            
            {\tt lFalse \&\& c(f(), f(), f())}
            
            faria disparar chamadas de {\tt f()}, para a construção do
            vetor {\tt c(f(), f(), f())}
            
        \item Mas não em {\tt R} !
        
        \item A construção desse vetor foi adiada para ser feita em 
            paralelo com a aplicação do operador {\tt \&\&} aos elementos
            de {\tt lFalse}.
            
    \end{itemize}

\end{frame}


\begin{frame}{Interpretação dos resultados ({\em cont.})}
    \begin{itemize}
        \item O fato da criação do vetor ter sido feita em 
            paralelo mostra o grande poder da manipulação de vetores 
            em {\tt R}, que tem impacto até mesmo na geração de 
            operandos em expressões lógicas.
            
        \item Não esquecendo da ausência de {\em short circuiting}, 
            são essas características que tornam o {\tt R} uma 
            linguagem de programação muito peculiar.
    \end{itemize}

\end{frame}

\begin{frame}[allowframebreaks]{Referências}
    \begin{thebibliography}{99}
        \bibitem{rBooleanOperators}
            {\em r - Boolean operators \&\& and || - Stack Overflow}\\
            \url{https://stackoverflow.com/questions/6558921/boolean-operators-and} \\
            Visitado em 01/05/2021
            
        \bibitem{RparaProgramar}
            {\em R para programar}\\
            \url{https://www.youtube.com/playlist?list=PLdgRds52HXiq-YlpmnxSPV9QgHEX5ggJ3}\\
            Visitado em 01/05/2021
                        
        \bibitem{R4prog_vetorizados}
            {\em R para programar, cap. 2 - Expressões lógicas no R 3/6}\\
            \url{https://youtu.be/UMts8qhAWX8}\\
            Visitado em 01/05/2021            
                        
        \bibitem{R4prog_longos}
            {\em R para programar, cap. 2 - Expressões lógicas no R 4/6}\\
            \url{https://youtu.be/iDOr8Fcnf2A}\\
            Visitado em 01/05/2021            
            
    \end{thebibliography}

\end{frame}

\end{document}
