# Benchmarking `eps` search for all possible orders of magnitude

# TODO get the number of magnitudes from command line, or from a data file

library("microbenchmark") # Benchmark de pequenos códigos

calc.eps <- function(base) {
  eps <- base
  while ((base + eps) != base) {
   eps <- eps / 2
  }
  
  eps
}

calc.significant.digits <- function (base, eps) {
  significant.digits <- -trunc(log10(eps / base))
  
  as.integer(significant.digits) 
}

main_loop <- function(n.magnitudes =  615L) {
  n.magnitudes <- n.magnitudes + (! as.logical(n.magnitudes %% 2L))
  
  base <- double(n.magnitudes)
  eps <- double(n.magnitudes)
  n.digits <- integer(n.magnitudes)
  
  mid.ind <- (n.magnitudes %/% 2L) + 1L
  upper.ind <- mid.ind + 1L
  lower.ind <- mid.ind - 1L
  
  base[mid.ind] <- 1
  eps[mid.ind] <- calc.eps(base[mid.ind])
  n.digits[mid.ind] <- calc.significant.digits(base[mid.ind], eps[mid.ind])
  
  while (lower.ind >= 1) {
    base[upper.ind] <- base[upper.ind - 1L] * 10
    base[lower.ind] <- base[lower.ind + 1L] / 10
    
    eps[upper.ind] <- calc.eps(base[upper.ind])
    eps[lower.ind] <- calc.eps(base[lower.ind])
    
    n.digits[upper.ind] <- 
      calc.significant.digits(base[upper.ind], eps[upper.ind])
    n.digits[lower.ind] <- 
      calc.significant.digits(base[lower.ind], eps[lower.ind])
    
    upper.ind <- upper.ind + 1L
    lower.ind <- lower.ind - 1L
  }
}

n.times <- 1000L

start <- Sys.time()
for (ind in 1:n.times) {
  main_loop()
}
stop <- Sys.time()

delta <- stop - start
print(delta)
cat("Average time:", delta / n.times, '\n\n')

bench.res = microbenchmark(main_loop(), times = n.times)
print(bench.res)
times <- bench.res$time / (1000 * 1000)
hist(times)
