# Anova for K-Means

#! /usr/bin/env R 

library("cluster")     # Basic functionality

anova.for.kmeans <- function(df, n.centers) {
  set.seed(123)
  km.res <- kmeans(df, n.centers, nstart = 25)

  dof.total  <- length(km.res$cluster) - 1
  dof.groups <- length(km.res$centers[, 1]) - 1
  dof.error  <- dof.total - dof.groups
  
  var.groups <- km.res$tot.withinss / dof.groups
  var.error  <- km.res$betweenss / dof.error
  
  f.value <- var.groups / var.error
  p.value <- pf(f.value, dof.groups, dof.error, lower.tail = FALSE)  
  
  anova <- list(f.value = f.value, dof.groups = dof.groups, 
                dof.error = dof.error, p.value = p.value)
  
  result <- list(km.res = km.res, anova = anova)
}

format.anova <- function(anova) {
  result <- paste("F-statistic: ", 
                  format(anova$f.value, width = 4), 
                  "on", anova$dof.groups,
                  "and", anova$dof.error,
                  "DF,  p-value: ", anova$p.value)
  
  result
}

data("USArrests")      # Loading the data set
df <- scale(USArrests) # Scaling the data

answer2 <- anova.for.kmeans(df, 2)
cat(format.anova(answer2$anova), '\n')

answer4 <- anova.for.kmeans(df, 4)
cat(format.anova(answer4$anova), '\n')
