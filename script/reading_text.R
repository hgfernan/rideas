# First steps for text mining, using MdA's A cartomante as data

library('tm') # Text mining package

#
#
# Download of document to process
#

# Creating the closure (a kind of function for PDF conversion)
pdfFunc <- readPDF(control = list(text = "-layout"))

# Reading the PDF 
txt <- pdfFunc(elem = list(uri = "../data/bv000257.pdf"), 
               language = "pt")

# Structure of the data read
str(txt)

# Structure of the content field
str(txt$content)

text <- txt$content

#
#
# Preparation of text -- 'cleaning'
#

lines <- strsplit(text[1], '\n')

# Structure of the split text
str(lines)

# Transforming lines into a character vector
lines <- lines[[1]]

# Locating the header
lines[1:10]

lines[11:20]

# Trimming the header
pag1 <- lines[17:length(lines)]

# Making the 1st page a unit vector
pag1 <- paste(pag1, collapse = '\n')

# How is the 1st page finished ?
print(text[1])

# Adding a '\n' to the 1st page
pag1 <- paste(pag1, '\n')

# A single expression that does it all
pag1 <- paste(paste(lines[17:length(lines)], collapse = '\n'), '\n')

# Replacing the 1st page with the one without the header
text[1] <- pag1

#
#
# Processing with tm
#

# Creating a virtual corpus for "A cartomante"
cartomante <- VCorpus(VectorSource(text))

cartomante <- tm_map(cartomante, content_transformer(tolower))

# Processing the virtual corpus & printing basic analysis
dtm <- DocumentTermMatrix(cartomante)
inspect(dtm)

# Transforming the text into a single vector
text.joined <- paste(text, collapse = '')
cartomante <- VCorpus(VectorSource(text.joined))
dtm <- DocumentTermMatrix(cartomante)
inspect(dtm)

# OBS no need to lower the words

# Removing the 'stop words'
cartomante <- tm_map(cartomante, removeWords, stopwords("portuguese"))
dtm <- DocumentTermMatrix(cartomante)
inspect(dtm)

# Removing the punctuation
cartomante <- tm_map(cartomante, removePunctuation)
dtm <- DocumentTermMatrix(cartomante)
inspect(dtm)




