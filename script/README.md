# Source files in the `script` folder


| File                          | Description                                                       | Video |
|:------------------------------|:----------------------------------------------------------------  |------:|
| `a4km_plot.R`                 | ANOVA test for a K-means clustering, with plotting                |
| `Anova4kmeans_simple.R`       | ANOVA test for a K-means clustering, 1st draft                    |
| `Anova4kmeans.R`              | ANOVA test for a K-means clustering, report only                  |
| `find_eps-bench_Rscript.Rout` | Redirected output of `find_eps-bench.R`, under `Rscript`          |
| `find_eps-bench.R`            | Benchmark for `find_eps.R`calculations                            |
| `find_eps-bench.Rout`         | Redirected output of `find_eps-bench.R`                           |
| `find_eps.R`                  | Find `eps` for several orders of magnitude                        |
| `find_eps.Rout`               | Redirected output of `find_eps.R`                                 |
| `freq_cloud.R`                | Generation of frequencies for word cloud & bar charts             |
| `gen_corpus.R`                | First steps for text mining                                       |
| `possible_lib.R`              | A possible library of functions                                   |
| `pdfToText.R`                 | Convert PDF files in a given directory to text                    |
| `reading_text.R`              | # First steps for text mining, using MdA's *A cartomante* as data |
| `script.Rproj`                | `rstudio` project file for this folder                            |
| `short_circuit.R`             | Test for *short-circuiting* of `&` and `&&` operators             | 

