# Corpus generation, from text files in a folder

library('tm')       # Text mining
library('hunspell') # Stemming

corpus.count.lines <- function(corpus) {
  result <- 0L
  
  for (ind in 1:length(corpus)) {
    result <- result + length(corpus[[ind]]$content)
  }
  
  result
}

# Global variables to let the user know the processing status 
total.corpus.lines  <- 0L # Total lines in the current corpus 
current.corpus.line <- 0L # Current line being processed from the current corpus

stemFuncOne <-function(textIn) {
  text <- trimws(textIn)
  if (nchar(text) == 0) {
    return(text)
  }

  twords.dirty <- strsplit(text, '[ ,.;:?!"]')[[1]]

  # OBS: hunspell_stem() has problems with some UTF8 chars.
  
  # To avoid null strings in twords.dirty
  twords <- character(0)
  for (tword.dirty in twords.dirty) {
    tword <- trimws(tword.dirty)
    if (nchar(tword) == 0) {
      next
    }

    for (tword_part in tword) {
      twords <- c(twords, tword_part)
    }
  }

  stemmed_list <- hunspell_stem(twords, dict = dictionary("pt_BR"))

  # OBS hunspell_stem() can returns a list of words, instead of just one
  # TODO see what causes this -- hunspell_stem() may be in doubt among them
  words <- character(0)
  for (sl.entry in stemmed_list) {
    
    for (entry in sl.entry) {
      if (nchar(entry) == 0) {
        next
      }
      words <- c(words, entry)
    }
  }

  (result <- paste(words, collapse = ' '))
}

stemFunc <- function(text) {
  
  result <- character(0)
  for (ind in 1:length(text)) {
    result <- c( result, stemFuncOne(text[ind]) )
  }
  
  current.corpus.line <<- current.corpus.line + length(text)
  
  chr.total <- as.character(total.corpus.lines)
  line <- format(Sys.time()) 
  line <- paste(line, ': Processed line')
  line <- paste( line, format(current.corpus.line, width = nchar(chr.total)) )
  line <- paste(line, "of", chr.total)  
  cat(line, '\n', sep = '')
    
  result
}


gen.vcorpus <- function(corpus.base, do.stem = FALSE,
                        eliminate.stopwords = TRUE,
                        folder = '../data/', pattern = '^[demq].*\\.txt')
{
  corpus <- VCorpus(DirSource(directory = folder,
                              pattern = pattern,
                              encoding = 'UTF-8',
                              mode = 'text'), 
                    readerControl = list(language = "pt"))

  total.corpus.lines <<- corpus.count.lines(corpus)
    
  # Text lower casing
  corpus <- tm_map(corpus, content_transformer(tolower))
  
  if (eliminate.stopwords) {
    # Elimination of stop words
    corpus <- tm_map(corpus, removeWords, stopwords(kind = "pt"))
  }
  
  # Striping spaces of all types
  corpus <- tm_map(corpus, stripWhitespace)
  
  meta(corpus, tag = "name", type = "corpus") <- corpus.base
    
  # Stem using hunspell does it all
  if (do.stem) {
    corpus <- tm_map(corpus, content_transformer(stemFunc))
    
    meta(corpus, tag = "name", type = "corpus") <- 
      paste(corpus.base, ".stemmed", sep = '')
    
    return(corpus)
    
  } else {
    # Remove punctuation
    corpus <- tm_map(corpus,
                     removePunctuation, preserve_intra_word_dashes = TRUE)
  }
  
  corpus
}

save.vcorpus <- function(corpus, path = '../data/')
{
  corpus.name <- meta(corpus, "name")

  # TODO saving corpus
  filenames <- paste(corpus.name, "-", seq_along(corpus), ".corp", sep = "")
  
  writeCorpus(corpus, 
              path = path, 
              filenames = filenames)
  
  corpus
}

cat(format(Sys.time()), ": Started processing\n")

mac.novels.stemmed <- gen.vcorpus('mac.novels', do.stem = TRUE)
cat(format(Sys.time()), ": Done corpus generation\n")

save.vcorpus(mac.novels.stemmed)
cat(format(Sys.time()), ": Saved corpus\n")

cat(format(Sys.time()), ": Finished processing\n")
